#!/usr/bin/env python3
"""
    A script to render magnetic north, temperature and humidity on a
     Raspberry Pi Sense Hat's RGB LEDs

    Contributors:
        Aaron S. Crandall <acrandal@gmail.com> - 2020

    This is a working version, but not a great code base
    Written quickly while on vacation in New Orleans
"""

from sense_hat import SenseHat          # RPi Sense Hat library
from time import sleep

DEG_PER_PIXEL = 360.0 / 28.0            # Used in North projection

# Setup 3x3 Number font:
#  http://cargocollective.com/slowercase/3x3-Font-for-Nerds
font_zero   = [[1,1,1],[1,0,1],[1,1,1]]
font_one    = [[1,1,0],[0,1,0],[1,1,1]]
font_two    = [[1,1,0],[0,1,0],[0,1,1]]
font_three  = [[1,1,0],[0,1,1],[1,1,0]]
font_four   = [[1,0,1],[1,1,1],[0,0,1]]
font_five   = [[0,1,1],[0,1,0],[1,1,0]]
font_six    = [[1,0,0],[1,1,1],[1,1,1]]
font_seven  = [[1,1,1],[0,0,1],[0,0,1]]
font_eight  = [[1,1,1],[1,1,1],[1,1,1]]
font_nine   = [[1,1,1],[1,1,1],[0,0,1]]

font = [None] * 10
font[0] = font_zero
font[1] = font_one
font[2] = font_two
font[3] = font_three
font[4] = font_four
font[5] = font_five
font[6] = font_six
font[7] = font_seven
font[8] = font_eight
font[9] = font_nine

# Defined colors for LEDs
OFF = (0,0,0)
RED = (150, 0, 0)
GREEN = (0, 150, 0)
BLUE = (0, 0, 150)


""" Renders a single digit to the LED array
    loc = tuple in the form (x, y)
    digit = single digit 0..9
    color = tuple of (red, green, blue)
"""
def render_number(loc, digit, color):
    num_b = font[digit]
    for y in range(3):
        for x in range(3):
            curr_x = loc[0] + x
            curr_y = loc[1] + y
            if num_b[y][x] == 0:
                sense.set_pixel(curr_x, curr_y, OFF)
            elif num_b[y][x] == 1:
                sense.set_pixel(curr_x, curr_y, color)


def update_humidity(humidity):
    humidity = int(humidity)
    humidity_tens_loc = (1,4)
    humidity_ones_loc = (4,4)
    tens_digit = int(humidity / 10)
    ones_digit = humidity % 10

    render_number(humidity_tens_loc, tens_digit, GREEN)
    render_number(humidity_ones_loc, ones_digit, GREEN)


def update_temperature(temperature):
    temperature = int(temperature)
    temperature_tens_loc = (1,1)
    temperature_ones_loc = (4,1)
    tens_digit = int(temperature / 10)
    ones_digit = temperature % 10

    render_number(temperature_tens_loc, tens_digit, BLUE)
    render_number(temperature_ones_loc, ones_digit, BLUE)


def calc_pixel_loc(pixel_count):
    pixel_loc = (0,0)
    x = 0
    y = 0

    if( pixel_count <= 3 ):
        x = pixel_count + 4
    elif( pixel_count >= 24 ):
        x = pixel_count - 24
    elif( pixel_count >= 10 and pixel_count <= 17):
        x = 17 - pixel_count
    elif( pixel_count >= 4 and pixel_count <= 9):
        x = 7
    else:
        x = 0
    x = 7 - x               # Let he without sin cast the first stone

    if( pixel_count >= 4 and pixel_count <= 10 ):
        y = pixel_count - 3
    elif( pixel_count >= 18 and pixel_count <= 23):
        y = 7 - (pixel_count - 17)
    elif( pixel_count >= 10 and pixel_count <= 17):
        y = 7
    else:
        y = 0

    return (x, y)


def update_compass(sense, last_pixel_loc):
    north = sense.get_compass()
    pixel_count = int(north / DEG_PER_PIXEL)
    sense.set_pixel(last_pixel_loc[0], last_pixel_loc[1], OFF)
    last_pixel_loc = calc_pixel_loc(pixel_count)
    sense.set_pixel(last_pixel_loc[0], last_pixel_loc[1], RED)

    return last_pixel_loc


if __name__ == "__main__":
    sense = SenseHat()
    sense.clear()
    last_pixel_loc = (0,0)
    done = False

    while not done:
        last_pixel_loc = update_compass(sense, last_pixel_loc)

        temperature = sense.get_temperature()
        update_temperature(temperature)

        humidity = sense.get_humidity()
        update_humidity(humidity)

        sleep(1)

