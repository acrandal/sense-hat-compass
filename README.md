# Sense Hat Compass, Temperature, and Humidity

A short python script which uses the Raspberry Pi Sense Hat to determine magnetic North, temperature, and humidity then render them to the Sense Hat RGB LED array.

North is rendered by a red pixel lit up along the edge of the 8x8 LED array.
That is the direction of North from the center of the array.
The code is not designed for a proper angle sweep, but flattens the 360 degrees to the 28 pixels on the edge of the array with a uniform 360 / 28 degrees per pixel projection.
The compass (magenetometer) on this board updates quite slowly, so be patient.

Temperature shows up in blue numbers.

Humidity shows up in green numbers.

The numbers used are in a 3x3 font which can be found here:

    http://cargocollective.com/slowercase/3x3-Font-for-Nerds


## Dependencies:

Hardware:
* Raspberry Pi (I used a Model 3B, but any should work)
* Sense Hat:

    https://www.raspberrypi.org/products/sense-hat/

Library: sense_hat 
 * Install on Raspbian: apt install sense-hat
 * Depends: enable I2C

## Recommended:

Calibrate the Sense Hat:

    https://www.raspberrypi.org/documentation/hardware/sense-hat/

    https://www.raspberrypi.org/forums/viewtopic.php?f=104&t=109064&p=750616#p810193


## Run / Execute:

python3 sense-hat-compass.py

## Expected issues:

* The compass updates quite slowly, about 1/2 degree per second
* The temperature is affected by the RPi and runs about +10C over ambient (min)

## Contributors:

* Aaron S. Crandall <acrandal@gmail.com> - 2020

## License:

Creative Commons Attribution-ShareAlike 4.0 International

    https://creativecommons.org/licenses/by-sa/4.0/